# Ctest 

I finally figured out gitlab pages!

## Workflow

### Initialisation

* create repo
* do all work in master branch
* create gh-pages branch `$ git branch gh-pages`

````shell
$ git checkout gh-pages
$ emacs .gitlab-ci.yml
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  - echo "created public dir for html pages"
  artifacts:
    paths:
    - public
  only:
  - gh-pages
$ git add .gitlab-ci.yml index.html processing.min.js 
$ git commit -a -m "first gh-pages commit"
$ git push --set-upstream origin gh-pages
````

* return to master branch for general work

### Loop
1. do work in master branch
2. add files to master
3. commit to master
4. push to master
5. merge from master to gh-pages and push

    ````shell
    $ git checkout gh-pages
    $ git merge master
    $ git push
    ````
6. return to master branch for more work: `$ git checkout master`

# Viewing

The web visualization for repo named REPO:  `https://gratefulfrog.gitlab.io/REPO`
